<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Election extends Model
{
    protected $fillable = [
        'slug'
    ];

    public function figures()
    {
        return $this->belongsToMany(
            Figure::class,
            'election_figure',
            'election_id',
            'figure_id'
        );
    }
}
