<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Log;
use App\Policy;

use Auth;

class PolicyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['body_class'] = 'policy';
        $data['policies'] = (
            Policy::where('status', 1)
                ->orderBy('created_at', 'ASC')
                ->get()
        );

        return view('/pages/policy/policy-index', $data);
    }

    public function detail($slug)
    {
        $data['body_class'] = 'policy detail';
        $data['policy'] = (
            Policy::where('slug', $slug)
                ->where('status', 1)
                ->firstOrFail()
        );

        return view('/pages/policy/policy-detail', $data);
    }

    public function interactionCheck(Request $request)
    {
        $policy = Policy::where('uuid', $request->uuid)->firstOrFail();
        $user = Auth::user();

        if ($user->choosePolicy($policy)) {
            $data['status'] = true;
        } else {
            $data['status'] = false;
        }

        return $data;
    }

    public function interaction(Request $request)
    {
        $policy = Policy::where('uuid', $request->uuid)->firstOrFail();
        $user = Auth::user();

        if (!$user->choosePolicy($policy) && $request->option == 1) {
            $policy->choose()->attach($user, ['option' => 1]);
        } elseif (!$user->choosePolicy($policy) && $request->option == 2) {
            $policy->choose()->attach($user, ['option' => 2]);
        }

        $data = [
            'agree'    => count($policy->agree) ? number_format((count($policy->agree) / ((count($policy->agree) + count($policy->disagree))) * 100), 2, '.', ',') : '0.00',
            'disagree' => count($policy->disagree) ? number_format((count($policy->disagree) / ((count($policy->disagree) + count($policy->agree))) * 100), 2, '.', ',') : '0.00',
            'option'   => $request->option
        ];

        $log = new Log;
        $log = Log::create([
            'uuid'          => getUUID(),
            'user_id'       => $user->id,
            'category_id'   => $policy->id,
            'category_type' => 2,
            'option'        => $request->option
        ]);

        return $data;
    }
}
