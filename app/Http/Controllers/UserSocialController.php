<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\UserSocial;

use Auth;
use Socialite;

class UserSocialController extends Controller
{
    public function redirectToProvider(Request $request)
    {
        if (env('APP_ENV') == 'production') {
            return redirect()->route('home');
        }

        if ($request->provider === 'facebook') {
            return Socialite::driver($request->provider)
                ->scopes([
                    'email', 'user_birthday'
                ])->redirect();
        } else if ($request->provider === 'google') {
            return Socialite::driver($request->provider)
                ->redirect();
        } else {
            return redirect()
                ->route('login');
        }
    }

    public function handleProviderCallback(Request $request)
    {
        if ($request->error) {
            return redirect()
                ->route('login');
        } else if ($request->provider === 'facebook') {
            $userProvider = (
                Socialite::driver($request->provider)
                    ->fields([
                        'id', 'name', 'email', 'gender', 'birthday'
                    ])->user()
            );

            $userProvider = [
                'id'         => isset($userProvider->id) ? $userProvider->id : NULL,
                'avatar'     => isset($userProvider->avatar_original) ? $userProvider->avatar_original : NULL,
                'name'       => isset($userProvider->name) ? $userProvider->name : NULL,
                'email'      => isset($userProvider->email) ? $userProvider->email : NULL,
                'birth_date' => isset($userProvider->user['birthday']) ? date('d M Y', strtotime($userProvider->user['birthday'])) : NULL,
                'gender'     => isset($userProvider->user['gender']) ? $userProvider->user['gender'] : NULL
            ];
        } else if ($request->provider === 'google') {
            $userProvider = (
                Socialite::driver($request->provider)
                    ->user()
            );

            $userProvider = [
                'id'         => isset($userProvider->id) ? $userProvider->id : NULL,
                'avatar'     => isset($userProvider->avatar_original) ? $userProvider->avatar_original : NULL,
                'name'       => isset($userProvider->name) ? $userProvider->name : NULL,
                'email'      => isset($userProvider->email) ? $userProvider->email : NULL,
                'birth_date' => isset($userProvider->user['birthday']) ? date('d M Y', strtotime($userProvider->user['birthday'])) : NULL,
                'gender'     => isset($userProvider->user['gender']) ? $userProvider->user['gender'] : NULL
            ];
        }

        if ($userProvider['id']) {
            $userSocial = (
                UserSocial::where('provider_id', $userProvider['id'])
                    ->where('provider', $request->provider)
                    ->first()
            );

            $user = (
                User::where('email', $userProvider['email'])
                    ->first()
            );

            if ($userSocial) {
                Auth::login($userSocial->user);

                if (!session()->has('from')) {
                    session()->put('from', url()->previous());
                }

                return redirect(session()->pull('from'));
            } else if ($user && !$userSocial) {
                $userSocial = new UserSocial([
                    'provider_id' => $userProvider['id'],
                    'provider'    => $request->provider
                ]);

                $userSocial->user()->associate($user);
                $userSocial->save();

                Auth::login($userSocial->user);

                if (!session()->has('from')) {
                    session()->put('from', url()->previous());
                }

                return redirect(session()->pull('from'));
            } else {
                return redirect()
                    ->route('register', [
                        'provider_id' => $userProvider['id'],
                        'provider'    => $request->provider,
                        'avatar'      => $userProvider['avatar'],
                        'name'        => $userProvider['name'],
                        'email'       => $userProvider['email'],
                        'birth_date'  => $userProvider['birth_date'],
                        'gender'      => $userProvider['gender']
                    ])->withErrors([
                        'birth_place' => 'Tempat Lahir dibutuhkan.',
                        'birth_date'  => $userProvider['birth_date'] ? '' : 'Tanggal Lahir dibutuhkan.',
                        'gender'      => $userProvider['gender'] ? '' : 'Jenis Kelamin dibutuhkan.',
                        'phone'       => 'Telepon dibutuhkan.',
                        'regency'     => 'Domisili dibutuhkan.'
                    ]);
            }
        } else {
            return redirect()
                ->route('login');
        }
    }
}
