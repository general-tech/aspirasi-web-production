<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Log;
use App\User;

use Auth;
use Cloudder;
use DB;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['body_class'] = 'user';
        $data['user'] = Auth::user();
        $data['logs'] = Log::where('user_id', Auth::user()->id)->get();

        return view('pages/user/user-index', $data);
    }

    public function edit()
    {
        $data['body_class'] = 'user edit';
        $data['method'] = 'PATCH';
        $data['user'] = Auth::user();
        $data['regencies'] = DB::select('SELECT * FROM `regencies`');

        return view('pages/user/user-edit', $data);
    }

    public function update(Request $request)
    {
        $request['birth_date']  = $request->birth_date ? date('Y-m-d', strtotime($request->birth_date)) : NULL;

        $messages = [
            'name.required'        => 'Nama Lengkap dibutuhkan.',
            'birth_place.required' => 'Tempat Lahir dibutuhkan.',
            'birth_date.required'  => 'Tanggal Lahir dibutuhkan.',
            'gender.required'      => 'Jenis Kelamin dibutuhkan.',
            'phone.required'       => 'Telepon dibutuhkan.',
            'phone.max'            => 'Telepon maksimal :max karakter.',
            'regency.required'     => 'Wilayah dibutuhkan.'
        ];

        $this->validate($request, [
            'avatar'      => 'sometimes|image|mimes:jpeg,png|file|max:1024',
            'name'        => 'required|string|max:255',
            'birth_place' => 'required|string|max:255',
            'birth_date'  => 'required|date',
            'gender'      => 'required|string',
            'phone'       => 'required|string|max:15',
            'regency'     => 'required|integer'
        ], $messages);

        if ($request->file('avatar')) {
            if (Auth::user()->avatar_id) {
                Cloudder::delete(Auth::user()->avatar_id);
            }

            Cloudder::upload(
                $request->file('avatar'),
                $publicId = 'user/' . Auth::user()->uuid . '-avatar',
                $option = array(
                    "transformation" => array(
                        array(
                            "width"  => 210,
                            "height" => 210,
                            "crop"   => "fill"
                        )
                    )
                )
            );

            $request['avatar_id'] = Cloudder::getResult()['public_id'];
            $request['avatar_url'] = Cloudder::getResult()['url'];

        }

        if ($request->file('idCard')) {
            if (Auth::user()->idCard_id) {
                Cloudder::delete(Auth::user()->idCard_id);
            }

            Cloudder::upload(
                $request->file('idCard'),
                $publicId = 'user/' . Auth::user()->uuid . '-idCard',
                $option = array(
                    "transformation" => array(
                        array(
                            "width"  => 210,
                            "crop"   => "fill"
                        )
                    )
                )
            );

            $request['idCard_id'] = Cloudder::getResult()['public_id'];
            $request['idCard_url'] = Cloudder::getResult()['url'];
        }

        $request['regency_id']  = $request->regency;

        User::where('uuid', Auth::user()->uuid)
            ->update($request->except(['_method', '_token', 'avatar', 'idCard', 'email', 'regency']));

        return redirect()
            ->route('profil');
    }
}
