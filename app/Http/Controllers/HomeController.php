<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\News;

use Auth;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'welcome']]);
    }

    public function index()
    {
        if (Auth::check()) {
            return $this->home();
        } else {
            return $this->welcome();
        }
    }

    public function welcome()
    {
        $data['body_class'] = 'welcome';

        return view('welcome', $data);
    }

    public function home()
    {
        $data['body_class'] = 'home';
        $data['latestNews'] = (
            News::where('status', 1)
                ->orderBy('created_at', 'ASC')
                ->take(3)
                ->get()
        );
        $data['news'] = (
            News::where('status', 1)
                ->orderBy('created_at', 'ASC')
                ->skip(3)
                ->take(News::count() > 3 ? News::count() - 3 : 0)
                ->get()
        );

        return view('home', $data);
    }

    public function news($slug)
    {
        $data['body_class'] = 'news detail';
        $data['theNews'] = (
            News::where('slug', $slug)
                ->where('status', 1)
                ->firstOrFail()
        );

        return view('/pages/news/news-detail', $data);
    }
}
