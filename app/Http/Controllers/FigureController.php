<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Election;
use App\Figure;
use App\Log;

use Auth;

class FigureController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['body_class'] = 'election';
        $data['elections'] = (
            Election::where('status', 1)
                ->orderBy('created_at', 'ASC')
                ->get()
        );

        return view('pages/election/election-index', $data);
    }

    public function detail($electionSlug)
    {
        $data['body_class'] = 'election detail';
        $data['election'] = (
            Election::where('slug', $electionSlug)
                ->where('status', 1)
                ->firstOrFail()
        );

        $figure_id = [];
        $data['election_opt'] = '';

        foreach ($data['election']->figures as $figure) {
            $figure_id[] = $figure->id;
            $data['election_opt'] += count($figure->opt);
        }

        $data['check'] = Auth::user()->figures()
            ->whereIn('figure_id', $figure_id)
            ->count();

        return view('pages/election/election-detail', $data);
    }

    public function profil($electionUuid, $figureUuid)
    {
        $data['body_class'] = 'profil detail';
        $data['figure'] = (
            Figure::where('uuid', $figureUuid)
                ->where('status', 1)
                ->firstOrFail()
        );

        $figure_id = [];

        foreach ($data['figure']->election[0]->figures as $key => $value) {
            $figure_id[] = $value->id;
        }

        $data['check'] = Auth::user()->figures()
            ->whereIn('figure_id', $figure_id)
            ->count();

        return view('/pages/figure/figure-detail', $data);
    }

    public function interactionCheck(Request $request)
    {
        $figure = Figure::where('uuid', $request->uuid)->firstOrFail();
        $user = Auth::user();

        $figure_id = [];

        foreach ($figure->election[0]->figures as $key => $value) {
            $figure_id[] = $value->id;
        }

        $check = Auth::user()->figures()
            ->whereIn('figure_id', $figure_id)
            ->count();

        if ($check) {
            $data['status'] = true;
        } else {
            $data['status'] = false;
        }

        return $data;
    }

    public function interaction(Request $request)
    {
        $figure = Figure::where('uuid', $request->uuid)->firstOrFail();
        $user = Auth::user();

        if (!$user->chooseFigure($figure)) {
            $figure->opt()->attach($user);
            $data['status'] = true;
        }

        $log = new Log;
        $log = Log::create([
            'uuid'          => getUUID(),
            'user_id'       => $user->id,
            'category_id'   => $figure->id,
            'category_type' => 1,
            'option'        => 1
        ]);

        $election_opt = '';

        foreach ($figure->election[0]->figures as $value) {
            $election_opt += count($value->opt);
            $figures[] = $value;
        }

        foreach ($figures as $figure_opt) {
            $data['opt'][] = [
                'uuid' => $figure_opt->uuid,
                'count' => count($figure_opt->opt) ? number_format((count($figure_opt->opt) / $election_opt * 100), 2, '.', ',') : '0.00',
            ];
        }

        return $data;
    }
}
