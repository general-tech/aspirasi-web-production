<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;

class ForgotPasswordController extends Controller
{
    use SendsPasswordResetEmails;

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showLinkRequestForm()
    {
        $data['body_class'] = 'auth';

        return view('auth.passwords.email', $data);
    }

    protected function validateEmail(Request $request)
    {
        $messages = [
            'email.required' => 'Email dibutuhkan.'
        ];

        $this->validate($request, [
            'email' => 'required|email'
        ], $messages);
    }
}
