<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

use App\User;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        if (env('APP_ENV') == 'production') {
            return redirect()->route('home');
        }

        $data['body_class'] = 'auth';

        return view('auth.login', $data);
    }

    protected function validateLogin(Request $request)
    {
        $messages = [
            'email.required'       => 'Email dibutuhkan.',
            'password.required'    => 'Password dibutuhkan.',
            'password.min'         => 'Password minimal :min karakter.'
        ];

        $this->validate($request, [
            $this->username() => 'required|string',
            'password'        => 'required|string|min:6'
        ], $messages);
    }


    protected function sendFailedLoginResponse(Request $request)
    {
        $checkUser = User::where('email', $request->email)->first();
        $checkPassword = (
            User::where('email', $request->email)
                ->where('password', bcrypt($request->password))
                ->first()
        );

        if (!$checkUser) {
            throw ValidationException::withMessages([
                $this->username() => 'Akun tidak ditemukan.',
            ]);
        } else if ($checkUser && !$checkPassword) {
            throw ValidationException::withMessages([
                'password' => 'Password salah.',
            ]);
        }
    }
}
