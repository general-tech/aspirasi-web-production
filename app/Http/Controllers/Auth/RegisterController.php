<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use DB;

class RegisterController extends Controller
{
    use RegistersUsers;

    protected $redirectTo = '/';

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm()
    {
        if (env('APP_ENV') == 'production') {
            return redirect()->route('home');
        }

        $data['body_class'] = 'auth';
        $data['regencies'] = DB::select('SELECT * FROM `regencies`');

        return view('auth.register', $data);
    }

    protected function validator(array $data)
    {
        $messages = [
            'name.required'        => 'Nama Lengkap dibutuhkan.',
            'email.required'       => 'Email dibutuhkan.',
            'email.unique'         => 'Email telah terdaftar.',
            'password.required'    => 'Password dibutuhkan.',
            'password.min'         => 'Password minimal :min karakter.',
            'password.confirmed'   => 'Konfirmasi Password tidak cocok.',
            'birth_place.required' => 'Tempat Lahir dibutuhkan.',
            'birth_date.required'  => 'Tanggal Lahir dibutuhkan.',
            'gender.required'      => 'Jenis Kelamin dibutuhkan.',
            'phone.required'       => 'Telepon dibutuhkan.',
            'phone.max'            => 'Telepon maksimal :max karakter.',
            'phone.min'            => 'Telepon minimal :min karakter.',
            'regency.required'     => 'Domisili dibutuhkan.'
        ];

        if (isset($data['provider_id'])) {
            return Validator::make($data, [
                'name'        => 'required|string|max:255',
                'email'       => 'required|string|email|max:255|unique:users',
                'birth_place' => 'required|string|max:255',
                'birth_date'  => 'required|date',
                'gender'      => 'required|string',
                'phone'       => 'required|string|min:6|max:15',
                'regency'     => 'required|integer'
            ], $messages);
        } else {
            return Validator::make($data, [
                'name'        => 'required|string|max:255',
                'email'       => 'required|string|email|max:255|unique:users',
                'password'    => 'required|string|min:6|confirmed',
                'birth_place' => 'required|string|max:255',
                'birth_date'  => 'required|date',
                'gender'      => 'required|string',
                'phone'       => 'required|string|min:6|max:15',
                'regency'     => 'required|integer'
            ], $messages);
        }
    }

    protected function create(array $data)
    {
        return User::create([
            'uuid'        => getUUID(),
            'name'        => $data['name'],
            'email'       => $data['email'],
            'password'    => $data['password'] ? bcrypt($data['password']) : NULL,
            'birth_place' => $data['birth_place'],
            'birth_date'  => date('Y-m-d', strtotime($data['birth_date'])),
            'gender'      => $data['gender'],
            'phone'       => $data['phone'],
            'regency_id'  => $data['regency'],
            'status'      => 2,
            'created_by'  => 1,
            'updated_by'  => 1
        ]);
    }
}
