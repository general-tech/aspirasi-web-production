<?php

if (!function_exists('setActive')) {
    function setActive($routes, $class = ' active') {
        $count = 0;

        foreach ($routes as $key => $route) {
            $count += substr(Route::currentRouteName(), 0, strlen($route)) === $route;
        }

        if ($count > 0) {
            return $class;
        } else {
            return '';
        }
    }
}

if (!function_exists('getTitle')) {
    function getTitle($str, $max) {
        return str_limit($str, $limit = $max, $end = '..');
    }
}

if (!function_exists('getDescription')) {
    function getDescription($str, $max) {
        if ($max !== null) {
            return str_limit(str_replace('<br>', '', strip_tags($str)), $limit = $max, $end = '..');
        } else {
            return str_replace('<br>', '', strip_tags($str));
        }
    }
}

if (!function_exists('getUUID')) {
    function getUUID() {
        return DB::SELECT('SELECT UUID() AS UUID')[0]->UUID;
    }
}

if (!function_exists('getURL')) {
    function getURL($url) {
        $headers = @get_headers($url);

        if (strpos($headers[0],'404') === false) {
          return true;
        } else {
          return false;
        }
    }
}

if (!function_exists('checkRoute')) {
    function checkRoute($route) {
        return substr(Route::currentRouteName(), 0, strlen($route)) === $route;
    }
}

if (!function_exists('checkRoutes')) {
    function checkRoutes($routes) {
        $count = 0;

        foreach ($routes as $key => $route) {
            $count += substr(Route::currentRouteName(), 0, strlen($route)) === $route;
        }

        if ($count > 0) {
            return true;
        } else {
            return false;
        }
    }
}
