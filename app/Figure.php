<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Figure extends Model
{
    public function election()
    {
        return $this->belongsToMany(
            Election::class,
            'election_figure',
            'figure_id',
            'election_id'
        );
    }

    public function opt()
    {
        return $this->belongsToMany(
            User::class,
            'figure_user',
            'figure_id',
            'user_id'
        );
    }
}
