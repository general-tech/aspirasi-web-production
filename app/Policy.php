<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Policy extends Model
{
    public function choose()
    {
        return $this->belongsToMany(
            User::class,
            'policy_user',
            'policy_id',
            'user_id'
        );
    }

    public function agree()
    {
        return $this->belongsToMany(
            User::class,
            'policy_user',
            'policy_id',
            'user_id'
        )->wherePivot('option', 1);
    }

    public function disagree()
    {
        return $this->belongsToMany(
            User::class,
            'policy_user',
            'policy_id',
            'user_id'
        )->wherePivot('option', 2);
    }
}
