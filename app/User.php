<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'uuid', 'avatar_id', 'avatar_url', 'idCard_id', 'idCard_url', 'name', 'username', 'email', 'password', 'birth_place', 'birth_date', 'gender', 'phone', 'regency_id', 'status', 'created_by', 'updated_by'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function figures()
    {
        return $this->belongsToMany(
            Figure::class,
            'figure_user',
            'user_id',
            'figure_id'
        );
    }

    public function chooseFigure(Figure $figure)
    {
        return !! $this->figures()
            ->where('figure_id', $figure->id)
            ->count();
    }

    public function policies()
    {
        return $this->belongsToMany(
            Policy::class,
            'policy_user',
            'user_id',
            'policy_id'
        );
    }

    public function choosePolicy(Policy $policy)
    {
        return !! $this->policies()
            ->where('policy_id', $policy->id)
            ->count();
    }

    public function agreePolicy(Policy $policy)
    {
        return !! $this->policies()
            ->where('policy_id', $policy->id)
            ->where('option', 1)
            ->count();
    }

    public function disagreePolicy(Policy $policy)
    {
        return !! $this->policies()
            ->where('policy_id', $policy->id)
            ->where('option', 2)
            ->count();
    }
}
