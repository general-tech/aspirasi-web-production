<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $fillable = [
        'uuid', 'user_id', 'category_id', 'category_type', 'option'
    ];

    public function figure()
    {
        return $this->belongsTo(
            Figure::class,
            'category_id'
        );
    }

    public function policy()
    {
        return $this->belongsTo(
            Policy::class,
            'category_id'
        );
    }
}
