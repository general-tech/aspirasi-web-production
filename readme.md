# Aspirasi Web

## Getting Started
### Prerequisites
* PHP >= 7.0.0
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension

### Installing
Configure .env file,
Drop all table in database,
```
composer update
php artisan migrate:refresh --seed
```

## Built With
* [Laravel](https://laravel.com/docs/5.4/) - The PHP Framework For Web Artisans | Version 5.5