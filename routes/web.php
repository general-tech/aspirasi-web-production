<?php

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::group(['prefix' => 'login'], function () {
    Route::get('/callback', 'UserSocialController@handleProviderCallback')->name('social.callback');
    Route::get('/redirect', 'UserSocialController@redirectToProvider')->name('social.redirect');
});

Route::group(['prefix' => 'kebijakan'], function () {
    Route::get('/', 'PolicyController@index')->name('kebijakan');
    Route::get('/{slug}', 'PolicyController@detail')->name('kebijakan.detail');
    Route::patch('/interaction/check', 'PolicyController@interactionCheck')->name('kebijakan.interaction.check');
    Route::patch('/interaction', 'PolicyController@interaction')->name('kebijakan.interaction');
});

Route::group(['prefix' => 'tokoh'], function () {
    Route::get('/', 'FigureController@index')->name('tokoh');
    Route::get('/{electionSlug}', 'FigureController@detail')->name('tokoh.detail');
    Route::get('/{electionUuid}/profil/{figureUuid}', 'FigureController@profil')->name('tokoh.profil');
    Route::patch('/interaction/check', 'FigureController@interactionCheck')->name('tokoh.interaction.check');
    Route::patch('/interaction', 'FigureController@interaction')->name('tokoh.interaction');
});

Route::group(['prefix' => 'profil-ku'], function () {
    Route::get('/', 'UserController@index')->name('profil');
    Route::get('/ubah', 'UserController@edit')->name('profil.edit');
    Route::patch('/update', 'UserController@update')->name('profil.update');
});

Route::get('berita/{slug}', 'HomeController@news')->name('berita');
