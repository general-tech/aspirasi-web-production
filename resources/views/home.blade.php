@extends('layouts.app')

@section('content')
<div class="top">
    <div class="row no-gutters">
        @foreach ($latestNews as $key => $theLatestNews)
            @if ($key == 0)
                <div class="col-sm-6">
                    <div class="box-post">
                        <div class="img" style="background-image: url({{ $theLatestNews->thumbnail_url }});"></div>
                        <div class="desc">
                            <div class="date">{{ date('d F Y | H:i:s', strtotime($theLatestNews->created_at)) }}</div>
                            <div class="title"><a href="{{ route('berita', ['slug' => $theLatestNews->slug]) }}">{!! getTitle($theLatestNews->title, 50) !!}</a></div>
                        </div>
                    </div>
                </div>
            @elseif ($key == 1)
                <div class="col-sm-6">
                    <div class="box-post">
                        <div class="img" style="background-image: url({{ $theLatestNews->thumbnail_url }});"></div>
                        <div class="desc">
                            <div class="date">{{ date('d F Y | H:i:s', strtotime($theLatestNews->created_at)) }}</div>
                            <div class="title"><a href="{{ route('berita', ['slug' => $theLatestNews->slug]) }}">{!! getTitle($theLatestNews->title, 50) !!}</a></div>
                        </div>
                    </div>
            @else
                    <div class="box-post">
                        <div class="img" style="background-image: url({{ $theLatestNews->thumbnail_url }});"></div>
                        <div class="desc">
                            <div class="date">{{ date('d F Y | H:i:s', strtotime($theLatestNews->created_at)) }}</div>
                            <div class="title"><a href="{{ route('berita', ['slug' => $theLatestNews->slug]) }}">{!! getTitle($theLatestNews->title, 50) !!}</a></div>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
    </div>
    <div class="row no-gutters">
        <div class="col-12 col-sm-6 headline text-right">
            KABAR TERKINI
        </div>
    </div>
</div>
<div class="list">
    <div class="container">
        <div class="row justify-content-center">
            @foreach ($news as $theNews)
                <div class="col-12 col-sm-6 col-md-4">
                    <div class="card">
                        <div class="card-img">
                            <img class="card-img-top" src="{{ $theNews->thumbnail_url }}">
                            <div class="card-img-date text-right">{{ date('d F Y | H:i:s', strtotime($theNews->created_at)) }}</div>
                        </div>
                        <div class="card-body">
                            <a href="{{ route('berita', ['slug' => $theNews->slug]) }}">
                                <h5 class="card-title">{!! getTitle($theNews->title, 50) !!}</h5>
                                <p class="card-text">{!! getDescription($theNews->description, 150) !!}</p>
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
