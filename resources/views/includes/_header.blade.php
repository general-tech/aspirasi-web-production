<nav class="navbar navbar-expand">
    <div class="container-fluid">
        <a class="navbar-brand m-0 p-0" href="{{ route('home') }}">
            @if (!Auth::check())
                <img class="d-block" src="{{ asset('img/logo-white.png') }}">
            @else
                <img class="d-none d-sm-block" src="{{ asset('img/logo-white.png') }}">
                <img class="d-block d-sm-none" src="{{ asset('img/logo-white-small.png') }}">
            @endif
        </a>
        <ul class="navbar-nav ml-auto">
            @if (Auth::check())
                <li class="nav-item">
                    <a class="nav-link ml-0 mr-2 mr-sm-3 my-1 p-0{{ setActive(['kebijakan']) }}" href="{{ route('kebijakan') }}" title="Kebijakan">Kebijakan</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link ml-0 mr-2 mr-sm-3 my-1 p-0{{ setActive(['tokoh']) }}" href="{{ route('tokoh') }}" title="Tokoh">Tokoh</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link ml-0 mr-2 mr-sm-3 my-1 p-0{{ setActive(['home', 'berita']) }}" href="{{ route('home') }}" title="Kabar Terkini">Kabar Terkini</a>
                </li>
                <li class="nav-item align-center">
                    <div class="divider"></div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link ml-2 ml-sm-3 mr-0 my-1 p-0{{ setActive(['profil']) }}" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="d-none d-sm-inline-block">{{ Auth::user()->name }}</span>
                        <img class="img-fluid rounded-circle ml-2" src="{{ Auth::user()->avatar_url ? : asset('img/avatar.jpg') }}" alt="">
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="{{ route('profil') }}">Profil Saya</a>
                        {{-- <a class="dropdown-item" href="{{ route('profil') }}">Ubah Password</a> --}}
                        <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                            Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </li>
            @else
                <li class="nav-item">
                    <a class="nav-link ml-0 mr-2 mr-sm-3 my-1 p-0{{ setActive(['login']) }}" href="{{ route('login') }}" title="Masuk">Masuk</a>
                </li>
                <li class="nav-item align-center">
                    <div class="divider"></div>
                </li>
                <li class="nav-item">
                    <a class="nav-link ml-2 ml-sm-3 mr-0 my-1 p-0{{ setActive(['register']) }}" href="{{ route('register') }}" title="Daftar">Daftar</a>
                </li>
            @endif
        </ul>
    </div>
</nav>
