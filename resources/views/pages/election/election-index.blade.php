@extends('layouts.app')

@section('content')
<div class="headline">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-11">
                <h1 class="m-0">Tokoh</h1>
                <p>Aspirasikan tokoh pilihanmu, demokratis untuk masa depan Indonesia.</p>
            </div>
        </div>
    </div>
</div>
<div class="list">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-11">
                @foreach ($elections as $election)
                    <div class="card">
                        <img class="card-img-top" src="{{ $election->thumbnail_url }}">
                        <div class="card-body">
                            <div class="card-title mb-4">{{ $election->title }}</div>
                            <div class="card-text">{{ getDescription($election->description, 250) }}</div>
                        </div>
                        <button class="btn btn-danger btn-block" type="button"
                            onclick="event.preventDefault();
                                     window.location.href = '{!! route('tokoh.detail', ['electionSlug' => $election->slug]) !!}';">
                                 Mari bersuara!
                        </button>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
