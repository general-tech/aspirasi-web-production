@extends('layouts.app')

@section('content')
<div class="headline"></div>
<div class="list">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-11">
                <div class="card">
                    <img class="card-img-top" src="{{ $election->thumbnail_url }}">
                    <div class="card-body">
                        <div class="card-title mb-4">{{ $election->title }}</div>
                        <div class="card-text">{{ getDescription($election->description, 250) }}</div>
                        <div class="figures">
                            <div class="row justify-content-center">
                                <div class="col-12">
                                    <div class="row justify-content-center">
                                        @foreach ($election->figures as $figure)
                                            <div id="figure_{{ $figure->uuid }}" class="col-12 col-sm-6 list-figure">
                                                <div class="row justify-content-center no-gutters">
                                                    <div class="col-7 text">
                                                        <a href="">
                                                            <div class="title mb-1">{{ $figure->name }}</div>
                                                            <div class="desc">{!! getDescription($figure->description, 200) !!}</div>
                                                        </a>
                                                        <div class="button">
                                                            @if ($check)
                                                                @if (Auth::user()->chooseFigure($figure))
                                                                    <button class="btn btn-success btn-block" type="button" disabled>Anda memilih <i class="fa fa-check"></i></button>
                                                                @else
                                                                    <button class="btn btn-danger btn-block" type="button" disabled>Pilih</button>
                                                                @endif
                                                            @else
                                                                <button class="btn btn-danger btn-block" type="button" data-uuid="{{ $figure->uuid }}"
                                                                    onclick="opt(this);">
                                                                    Pilih
                                                                </button>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-5">
                                                        <img class="img-fluid" src="{{ Cloudder::show('tokoh/' . $figure->uuid) }}">
                                                        <span class="badge badge-danger">{{ count($figure->opt) ? number_format((count($figure->opt) / $election_opt * 100), 2, '.', ',') : '0.00' }}%</span>

                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page_script')
    <script type="text/javascript">
        function opt(data) {
            @if (!Auth::check())
                window.location.replace('{{ route('login') }}');
            @elseif (!Auth::user()->idCard_id)
                alert('Harap upload KTP untuk mulai memilih.');
            @elseif (Auth::user()->status != 1)
                alert('KTP kamu masih dalam proses validasi.');
            @else
                uuid = $(data).data('uuid');

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                })

                $.ajax({
                    url: '{{ route('tokoh.interaction.check') }}',
                    type: 'PATCH',
                    data: {
                        'uuid': uuid
                    },
                    success: function(data) {
                        if (data.status == false) {
                            if (confirm('Apakah anda memilih tokoh ini?')) {
                                $.ajaxSetup({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    }
                                })

                                $.ajax({
                                    url: '{{ route('tokoh.interaction') }}',
                                    type: 'PATCH',
                                    data: {
                                        'uuid': uuid
                                    },
                                    success: function(data) {
                                        if (data.status == true) {
                                            $('.button').empty().append(
                                                "<button class='btn btn-danger btn-block' type='button' disabled>Pilih</button>"
                                            );

                                            $("#figure_" + uuid + " .button").empty().append(
                                                "<button class='btn btn-success btn-block' type='button' disabled>Anda memilih <i class='fa fa-check'></i></button>"
                                            );

                                            for (var i = 0, len = data.opt.length; i < len; i++) {
                                                $("#figure_" + data.opt[i]['uuid'] + " .badge").html(data.opt[i]['count'] + '%');
                                            }
                                        }
                                    }
                                })
                            }
                        }
                    }
                })

            @endif
        }
    </script>
@endsection
