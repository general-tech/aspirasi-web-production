@extends('layouts.app')

@section('content')
<div class="headline"></div>
<div class="list">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-11">
                <div class="card">
                    <img class="card-img-top" src="{{ $policy->thumbnail_url }}">
                    <div class="card-body">
                        <div class="card-title mb-4">{{ $policy->title }}</div>
                        <div class="card-text mb-5">{!! $policy->description !!}</div>
                        <div id="policy_{{ $policy->uuid }}" class="row justify-content-center align-items-center">
                            <div class="col-6">
                                <div class="row justify-content-center count">
                                    <div class="col-6 align-self-center text-center">
                                        <div class="media">
                                            <img class="img-fluid mr-3 align-self-center" width="40" src="{{ asset('/img/agree.png') }}">
                                            <div class="media-body align-self-center text-left agree">
                                                <h1 class="m-0">{{ count($policy->agree) ? number_format((count($policy->agree) / ((count($policy->agree) + count($policy->disagree))) * 100), 2, '.', ',') : '0.00' }}%</h1>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 align-self-center text-center">
                                        <div class="media">
                                            <img class="img-fluid mr-3 align-self-center" width="40" src="{{ asset('/img/disagree.png') }}">
                                            <div class="media-body align-self-center text-left disagree">
                                                <h1 class="m-0">{{ count($policy->disagree) ? number_format((count($policy->disagree) / ((count($policy->disagree) + count($policy->agree))) * 100), 2, '.', ',') : '0.00' }}%</h1>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-5 offset-1">
                                <div class="row justify-content-center button">
                                    @if (Auth::user()->choosePolicy($policy))
                                        @if (Auth::user()->agreePolicy($policy))
                                            <div class="col-12">
                                                <button class="btn btn-success btn-block" type="button" disabled>Anda setuju</button>
                                            </div>
                                        @elseif (Auth::user()->disagreePolicy($policy))
                                            <div class="col-12">
                                                <button class="btn btn-danger btn-block" type="button" disabled>Anda tidak setuju</button>
                                            </div>
                                        @endif
                                    @else
                                        <div class="col-6">
                                            <button class="btn btn-success btn-block" type="button" data-option="1" data-uuid="{{ $policy->uuid }}"
                                                onclick="opt(this);">
                                                Setuju
                                            </button>
                                        </div>
                                        <div class="col-6">
                                            <button class="btn btn-danger btn-block" type="button" data-option="2" data-uuid="{{ $policy->uuid }}"
                                                onclick="opt(this);">
                                                Tidak setuju
                                            </button>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page_script')
    <script type="text/javascript">
        function opt(data) {
            @if (!Auth::check())
                window.location.replace('{{ route('login') }}');
            @elseif (!Auth::user()->idCard_id)
                alert('Harap upload KTP untuk mulai memilih.');
            @elseif (Auth::user()->status != 1)
                alert('KTP kamu masih dalam proses validasi.');
            @else
                option = $(data).data('option');
                uuid = $(data).data('uuid');


                if (option == 1) {
                    text = 'Apakah anda setuju dengan kebijakan ini?';
                } else if (option == 2) {
                    text = 'Apakah anda tidak setuju dengan kebijakan ini?';
                }

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                })

                $.ajax({
                    url: '{{ route('kebijakan.interaction.check') }}',
                    type: 'PATCH',
                    data: {
                        'uuid': uuid
                    },
                    success: function(data) {
                        if (data.status == false) {
                            if (confirm(text)) {
                                $.ajaxSetup({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    }
                                })

                                $.ajax({
                                    url: '{{ route('kebijakan.interaction') }}',
                                    type: 'PATCH',
                                    data: {
                                        'uuid': uuid,
                                        'option': option
                                    },
                                    success: function(data) {
                                        if (data.option == 1) {
                                            $("#policy_" + uuid + " .button").empty().append(
                                                "<div class='col-12'> \
                                                    <button class='btn btn-success btn-block' type='button' disabled>Anda setuju</button> \
                                                </div>"
                                            );
                                        } else if (data.option == 2) {
                                            $("#policy_" + uuid + " .button").empty().append(
                                                "<div class='col-12'> \
                                                    <button class='btn btn-danger btn-block' type='button' disabled>Anda tidak setuju</button> \
                                                </div>"
                                            );
                                        }

                                        $("#policy_" + uuid + " .count .agree").empty().append(
                                            "<h1 class='m-0'> \
                                                " + data.agree + "% \
                                            </h1>"
                                        );

                                        $("#policy_" + uuid + " .count .disagree").empty().append(
                                            "<h1 class='m-0'> \
                                                " + data.disagree + "% \
                                            </h1>"
                                        );
                                    }
                                })

                            }
                        }
                    }
                })

            @endif
        }
    </script>
@endsection
