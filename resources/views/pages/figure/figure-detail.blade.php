@extends('layouts.app')

@section('content')
<div class="headline">
    <div class="container">
        <h1 class="m-0">{{ $figure->name }}</h1>
    </div>
</div>
<div class="list">
    <div class="container">
        <div class="card">
            <img class="card-img-top" src="{{ Cloudder::show('/tokoh/' . $figure->uuid) }}">
            <div class="card-body">
                <div class="card-text">{!! $figure->description !!}</div>
                <div class="row align-items-center">
                    <div class="col-4 offset-1 button">
                        @if ($check)
                            @if (Auth::user()->chooseFigure($figure))
                                <button class="btn btn-success btn-block" type="button" disabled>Anda memilih <i class="fa fa-check"></i></button>
                            @else
                                <button class="btn btn-danger btn-block" type="button" disabled>Pilih</button>
                            @endif
                        @else
                            <button class="btn btn-danger btn-block" type="button" data-uuid="{{ $figure->uuid }}"
                                onclick="opt(this);">
                                Pilih
                            </button>
                        @endif
                    </div>
                    <div class="col-6 offset-1">
                        <h1 class="m-0 text-center count">{{ count($figure->opt) }} Pemilih</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page_script')
    <script type="text/javascript">
        function opt(data) {
            @if (!Auth::check())
                window.location.replace('{{ route('login') }}');
            @elseif (!Auth::user()->idCard_id)
                alert('Harap upload KTP untuk mulai memilih.');
            @elseif (Auth::user()->status != 1)
                alert('KTP kamu masih dalam proses validasi.');
            @else
                uuid = $(data).data('uuid');

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                })

                $.ajax({
                    url: '{{ route('tokoh.interaction.check') }}',
                    type: 'PATCH',
                    data: {
                        'uuid': uuid
                    },
                    success: function(data) {
                        if (data.status == false) {
                            if (confirm('Apakah anda memilih tokoh ini?')) {
                                $.ajaxSetup({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    }
                                })

                                $.ajax({
                                    url: '{{ route('tokoh.interaction') }}',
                                    type: 'PATCH',
                                    data: {
                                        'uuid': uuid
                                    },
                                    success: function(data) {
                                        if (data.status == true) {
                                            $('.button').empty().append(
                                                "<button class='btn btn-success btn-block' type='button' disabled>Anda memilih <i class='fa fa-check'></i></button>"
                                            );

                                            $('.count').html(data.opt + ' Pemilih');
                                        }
                                    }
                                })
                            }
                        }
                    }
                })

            @endif
        }
    </script>
@endsection
