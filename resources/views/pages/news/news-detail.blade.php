@extends('layouts.app')

@section('content')
<div class="headline" style="background-image: url({{ $theNews->thumbnail_url }});"></div>
<div class="list">
    <div class="container">
        <div class="card">
            <div class="card-body">
                <div class="card-date">{{ date('d F Y | H:i:s', strtotime($theNews->created_at)) }}</div>
                <div class="card-title">{!! $theNews->title !!}</div>
                <div class="card-text">{!! $theNews->description !!}</div>
            </div>
        </div>
    </div>
</div>
@endsection
