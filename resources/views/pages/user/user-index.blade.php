@extends('layouts.app')

@section('content')
<div class="headline"></div>
<div class="list">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-11 col-lg-10 col-xl-9">
                <div class="card">
                    <div class="row justify-content-center">
                        <div class="col-11 col-sm-10">
                            <div class="media text-center text-sm-left user">
                                <img class="rounded-circle mb-4 mb-sm-0" src="{{ $user->avatar_url ? : asset('img/avatar.jpg') }}">
                                <div class="media-body align-self-center">
                                    <span class="d-block name">{{ $user->name }}</span>
                                    @if ($user->birth_place || $user->birth_date)
                                        <span class="d-block birth">
                                            {{ $user->birth_place ? $user->birth_place . ' | ' : '' }}{{ $user->birth_date ? date('d F Y', strtotime($user->birth_date)) : '' }}
                                        </span>
                                    @endif
                                    <a href="{{ route('profil.edit') }}" class="btn btn-danger"><i class="fa fa-gear"></i> Ubah Profil</a>
                                    {{-- <a href="{{ route('profil.password') }}" class="btn btn-danger"><i class="fa fa-lock"></i> Ubah Password</a> --}}
                                </div>
                            </div>
                        </div>
                        <div class="col-11 col-sm-10">
                            <div class="card-body">
                                <div class="card-title">
                                    <span class="bg-white pr-3">Aktivitas Saya</span>
                                </div>
                                <div class="row justify-content-center">
                                    <div class="col-12 col-sm-11">
                                        <ul class="list-unstyled m-0">
                                            @if (count($logs))
                                                @foreach ($logs as $log)
                                                    @if ($log->category_type == 1)
                                                        <li class="media p-0">
                                                            <img class="rounded-circle align-self-center" src="{{ asset('img/star.png') }}">
                                                            <div class="media-body align-self-center">
                                                                <span class="d-block title">{{ $log->figure->name }}</span>
                                                                {{-- <span class="desc d-none d-sm-block">ABC</span> --}}
                                                            </div>
                                                        </li>
                                                    @elseif ($log->category_type == 2)
                                                        <li class="media p-0">
                                                            <img class="rounded-circle align-self-center" src="{{ asset('img') . ($log->option == 1 ? '/checked' : '/unchecked') . '.png' }}">
                                                            <div class="media-body align-self-center">
                                                                <span class="d-block title">{{ $log->policy->title }}</span>
                                                                {{-- <span class="desc d-none d-sm-block">ABC</span> --}}
                                                            </div>
                                                        </li>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
