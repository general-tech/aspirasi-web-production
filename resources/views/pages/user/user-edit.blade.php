@extends('layouts.app')

@section('content')
<div class="headline"></div>
<div class="list">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-11 col-lg-10 col-xl-9">
                <div class="card">
                    <div class="row justify-content-center">
                        <div class="col-12 col-sm-10">
                            <div class="row justify-content-center upload">
                                <div class="col-6 mb-4 text-center">
                                    <img id="avatar" class="img-fluid rounded-circle" src="{{ $user->avatar_url ? : asset('img/avatar.jpg') }}">
                                </div>
                                <div class="col-6 align-self-center mb-4">
                                    <button id="input_avatar" class="btn btn-danger btn-block" type="button">{{ $user->avatar_id ? 'Ubah Foto' : 'Upload Foto' }}</button>
                                </div>
                                <div class="col-6 text-center">
                                    <img id="idCard" class="img-fluid" src="{{ $user->idCard_url ? : asset('img/idCard.jpg') }}">
                                </div>
                                @if (!$user->idCard_id)
                                    <div class="col-6 align-self-center">
                                        <div class="media-body align-self-center">
                                            <button id="input_idCard" class="btn btn-danger btn-block" type="button">Upload KTP</button>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-12 col-sm-10">
                            <div class="card-body">
                                <div class="row justify-content-center">
                                    <div class="col-12">
                                        <form class="form-horizontal" method="POST" action="{{ route('profil.update') }}" enctype="multipart/form-data">
                                            {{ method_field($method) }}
                                            {{ csrf_field() }}

                                            <input type="file" id="avatar" name="avatar" hidden>
                                            <input type="file" id="idCard" name="idCard" hidden>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-md-3 col-form-label align-self-center">Nama Lengkap</label>
                                                <div class="col-sm-8 col-md-9">
                                                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') ? : $user->name }}" placeholder="Nama Lengkap">
                                                    @if ($errors->has('name'))
                                                        <div class="invalid-feedback">
                                                            {{ $errors->first('name') }}
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-md-3 col-form-label align-self-center">Email</label>
                                                <div class="col-sm-8 col-md-9">
                                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') ? : $user->email }}" placeholder="Email" disabled>
                                                    @if ($errors->has('email'))
                                                        <div class="invalid-feedback">
                                                            {{ $errors->first('email') }}
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-md-3 col-form-label align-self-center">Tempat/Tanggal Lahir</label>
                                                <div class="col-sm-8 col-md-9">
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <input id="birth_place" type="text" class="form-control{{ $errors->has('birth_place') ? ' is-invalid' : '' }}" name="birth_place" value="{{ old('birth_place') ? : $user->birth_place }}" placeholder="Tempat Lahir">
                                                            @if ($errors->has('birth_place'))
                                                                <div class="invalid-feedback">
                                                                    {{ $errors->first('birth_place') }}
                                                                </div>
                                                            @endif
                                                        </div>
                                                        <div class="col-6">
                                                            <input id="birth_date" type="text" class="form-control datepicker{{ $errors->has('birth_date') ? ' is-invalid' : '' }}" name="birth_date" value="{{ old('birth_date') ? date('d F Y', strtotime(old('birth_date'))) : ($user->birth_date ? date('d F Y', strtotime($user->birth_date)) : '') }}" placeholder="Tanggal Lahir">
                                                            @if ($errors->has('birth_date'))
                                                                <div class="invalid-feedback">
                                                                    {{ $errors->first('birth_date') }}
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-md-3 col-form-label align-self-center">Jenis Kelamin</label>
                                                <div class="col-sm-8 col-md-9">
                                                    <select class="form-control select2{{ $errors->has('gender') ? ' is-invalid' : '' }}" name="gender" data-placeholder="Jenis Kelamin.."{{ app('request')->input('gender') ? ' disabled' : '' }}>
                                                        <option></option>
                                                        <option value="male"{{ old('gender') == 'male' ? ' selected' : ($user->gender == 'male' ? ' selected' : '') }}>Laki-laki</option>
                                                        <option value="female"{{ old('gender') == 'female' ? ' selected' : ($user->gender == 'female' ? ' selected' : '') }}>Perempuan</option>
                                                    </select>
                                                    @if ($errors->has('gender'))
                                                        <div class="invalid-feedback">
                                                            {{ $errors->first('gender') }}
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-md-3 col-form-label align-self-center">Telepon</label>
                                                <div class="col-sm-8 col-md-9">
                                                    <input id="phone" type="number" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') ? : $user->phone }}" placeholder="Telepon">
                                                    @if ($errors->has('phone'))
                                                        <div class="invalid-feedback">
                                                            {{ $errors->first('phone') }}
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-md-3 col-form-label align-self-center">Domisili</label>
                                                <div class="col-sm-8 col-md-9">
                                                    <select id="single" class="form-control select2{{ $errors->has('regency') ? ' is-invalid' : '' }}" name="regency" data-placeholder="Pilih Wilayah..">
                                                        <option></option>
                                                        @foreach ($regencies as $regency)
                                                            <option value="{{ $regency->id }}"{{ old('regency') == $regency->id ? ' selected' : ($user->regency_id == $regency->id ? ' selected' : '') }}>{{ ucwords(strtolower($regency->name)) }}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('regency'))
                                                        <div class="invalid-feedback">
                                                            {{ $errors->first('regency') }}
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group row justify-content-center">
                                                <div class="col-8">
                                                    <button type="submit" class="btn btn-danger btn-block">
                                                        Simpan
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page_script')
    <script type="text/javascript">
        $(".select2[name='gender']").select2({
            theme: 'bootstrap',
            placeholder: 'Jenis Kelamin..',
            allowClear: true
        });

        $(".select2[name='regency']").select2({
            theme: 'bootstrap',
            placeholder: 'Domisili..',
            allowClear: true
        });

        $('.datepicker').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd M yy',
            yearRange: '1950:2001',
            defaultDate: '01 Jan 1950'
        });

        function readURL(id, input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('img#' + id).attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $('input#avatar').change(function() {
            readURL('avatar', this);
        });

        $('input#idCard').change(function() {
            readURL('idCard', this);
        });

        $('#input_avatar').click(function(){
            $('input#avatar').click();
        });

        $('#input_idCard').click(function(){
            $('input#idCard').click();
        });
    </script>
@endsection
