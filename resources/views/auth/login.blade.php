@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-sm-11 col-md-9 col-lg-8 col-xl-7">
            <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}

                <h2 class="text-center m-0">Masuk</h2>
                <div class="form-group row justify-content-center">
                    <div class="col-6 col-sm-5 col-md-4">
                        <button type="button" class="btn btn-primary btn-block"
                            onclick="location.href='{{ route('social.redirect', ['provider' => 'facebook']) }}'">
                            Masuk dengan&nbsp;&nbsp;<i class="fa fa-facebook align-middle"></i>
                        </button>
                    </div>
                    <div class="col-6 col-sm-5 col-md-4">
                        <button type="button" class="btn btn-danger btn-block"
                    onclick="location.href='{{ route('social.redirect', ['provider' => 'google']) }}'">
                            Masuk dengan&nbsp;&nbsp;<i class="fa fa-google-plus align-middle"></i>
                        </button>
                    </div>
                </div>
                <div class="form-group row justify-content-center">
                    <div class="col-12 col-sm-10 col-md-8">
                        <div class="text-center or">
                            <span class="bg-white px-3">ATAU</span>
                        </div>
                    </div>
                </div>
                <div class="form-group row justify-content-center">
                    <div class="col-12 col-sm-10 col-md-8">
                        <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email">
                        @if ($errors->has('email'))
                            <div class="invalid-feedback">
                                {{ $errors->first('email') }}
                            </div>
                        @endif
                    </div>
                </div>
                <div class="form-group row justify-content-center">
                    <div class="col-12 col-sm-10 col-md-8">
                        <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password">
                        @if ($errors->has('password'))
                            <div class="invalid-feedback">
                                {{ $errors->first('password') }}
                            </div>
                        @endif
                    </div>
                </div>
                <div class="form-group row justify-content-center mb-2">
                    <div class="col-10 col-sm-8 col-md-6">
                        <button type="submit" class="btn btn-danger btn-block">
                            Masuk
                        </button>
                    </div>
                </div>
                <div class="form-group row justify-content-center">
                    <div class="col-10 col-sm-8 col-md-6">
                        <div class="checkbox">
                            <label class="m-0 noselect" role="button">
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Ingat Saya
                            </label>
                            {{-- <a class="btn btn-link p-0 float-right" href="{{ route('password.request') }}">
                                Lupa Password?
                            </a> --}}
                        </div>
                    </div>
                </div>
                <div class="form-group last">
                    <div class="row justify-content-center mb-0 py-3">
                        <div class="col-12 col-sm-10 col-md-8">
                            <div class="not-yet text-center">
                                Belum memiliki akun? <a class="btn btn-link p-0" href="{{ route('register') }}">Daftar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
