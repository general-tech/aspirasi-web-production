@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-7">
            <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                {{ csrf_field() }}

                <h2 class="text-center m-0">Lupa Password?</h2>
                <div class="form-group row justify-content-center status">
                    <div class="col-8">
                        @if (!session('status'))
                            <div class="alert alert-success mb-0" style="border-radius: 0;">
                                {{ session('status') }}a
                            </div>
                            <script type="text/javascript">
                                setTimeout(function() {
                                    $('.status').slideUp();
                                }, 3500);
                            </script>
                        @endif
                    </div>
                </div>
                <div class="form-group row justify-content-center">
                    <div class="col-8">
                        <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email">
                        @if ($errors->has('email'))
                            <div class="invalid-feedback">
                                {{ $errors->first('email') }}
                            </div>
                        @endif
                    </div>
                </div>
                <div class="form-group row justify-content-center">
                    <div class="col-6">
                        <button type="submit" class="btn btn-danger btn-block">
                            Kirim tautan reset password
                        </button>
                    </div>
                </div>
                <div class="form-group last">
                    <div class="row justify-content-center mb-0 py-3">
                        <div class="col-8">
                            <div class="not-yet text-center">
                                Sudah memiliki akun? <a class="btn btn-link p-0" href="{{ route('login') }}">Masuk</a>&nbsp;&nbsp;|&nbsp;&nbsp;Belum memiliki akun? <a class="btn btn-link p-0" href="{{ route('register') }}">Daftar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
