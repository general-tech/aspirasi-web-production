@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-sm-11 col-md-9 col-lg-8 col-xl-7">
            <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}

                <input type="hidden" name="provider_id" value="{{ app('request')->input('provider_id') ? : '' }}">
                <input type="hidden" name="provider" value="{{ app('request')->input('provider') ? : '' }}">
                <input type="hidden" name="avatar" value="{{ app('request')->input('avatar') ? : '' }}">
                <input type="hidden" name="birth_date" value="{{ app('request')->input('birth_date') ? : '' }}">
                <input type="hidden" name="gender" value="{{ app('request')->input('gender') ? : '' }}">

                <h2 class="text-center m-0">Daftar Akun</h2>
                @if (!app('request')->input('provider'))
                    <div class="form-group row justify-content-center">
                        <div class="col-6 col-sm-5 col-md-4">
                            <button type="button" class="btn btn-primary btn-block"
                                onclick="location.href='{{ route('social.redirect', ['provider' => 'facebook']) }}'">
                                Daftar dengan&nbsp;&nbsp;<i class="fa fa-facebook align-middle"></i>
                            </button>
                        </div>
                        <div class="col-6 col-sm-5 col-md-4">
                            <button type="button" class="btn btn-danger btn-block"
                                onclick="location.href='{{ route('social.redirect', ['provider' => 'google']) }}'">
                                Daftar dengan&nbsp;&nbsp;<i class="fa fa-google-plus align-middle"></i>
                            </button>
                        </div>
                    </div>
                    <div class="form-group row justify-content-center">
                        <div class="col-12 col-sm-10 col-md-8">
                            <div class="text-center or">
                                <span class="bg-white px-3">ATAU</span>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="form-group row justify-content-center">
                    <div class="col-12 col-sm-10 col-md-8">
                        <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') ? : (app('request')->input('name') ? : '') }}" placeholder="Nama Lengkap"{{ app('request')->input('name') ? ' readonly' : '' }}>
                        @if ($errors->has('name'))
                            <div class="invalid-feedback">
                                {{ $errors->first('name') }}
                            </div>
                        @endif
                    </div>
                </div>
                <div class="form-group row justify-content-center">
                    <div class="col-12 col-sm-10 col-md-8">
                        <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') ? : (app('request')->input('email') ? : '') }}" placeholder="Email"{{ app('request')->input('email') ? ' readonly' : '' }}>
                        @if ($errors->has('email'))
                            <div class="invalid-feedback">
                                {{ $errors->first('email') }}
                            </div>
                        @endif
                    </div>
                </div>
                @if (!app('request')->input('provider'))
                    <div class="form-group row justify-content-center">
                        <div class="col-12 col-sm-10 col-md-8">
                            <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password">
                            @if ($errors->has('password'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('password') }}
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row justify-content-center">
                        <div class="col-12 col-sm-10 col-md-8">
                            <input type="password" class="form-control" name="password_confirmation" placeholder="Konfirmasi Password">
                        </div>
                    </div>
                @endif
                <div class="form-group row justify-content-center">
                    <div class="col-6 col-sm-5 col-md-4">
                        <input type="text" class="form-control{{ $errors->has('birth_place') ? ' is-invalid' : '' }}" name="birth_place" value="{{ old('birth_place') }}" placeholder="Tempat Lahir">
                        @if ($errors->has('birth_place'))
                            <div class="invalid-feedback">
                                {{ $errors->first('birth_place') }}
                            </div>
                        @endif
                    </div>
                    <div class="col-6 col-sm-5 col-md-4">
                        <input type="text" class="form-control datepicker{{ $errors->has('birth_date') ? ' is-invalid' : '' }}" name="birth_date" value="{{ old('birth_date') ? : (app('request')->input('birth_date') ? date('d M Y', strtotime(app('request')->input('birth_date'))) : '') }}" placeholder="Tanggal Lahir">
                        @if ($errors->has('birth_date'))
                            <div class="invalid-feedback">
                                {{ $errors->first('birth_date') }}
                            </div>
                        @endif
                    </div>
                </div>
                <div class="form-group row justify-content-center">
                    <div class="col-12 col-sm-10 col-md-8">
                        <select class="form-control select2{{ $errors->has('gender') ? ' is-invalid' : '' }}" name="gender" {{ app('request')->input('gender') ? ' disabled' : '' }}>
                            <option></option>
                            <option value="male"{{ old('gender') == 'male' ? ' selected' : (app('request')->input('gender') == 'male' ? ' selected' : '') }}>Laki-laki</option>
                            <option value="female"{{ old('gender') == 'female' ? ' selected' : (app('request')->input('gender') == 'female' ? ' selected' : '') }}>Perempuan</option>
                        </select>
                        @if ($errors->has('gender'))
                            <div class="invalid-feedback">
                                {{ $errors->first('gender') }}
                            </div>
                        @endif
                    </div>
                </div>
                <div class="form-group row justify-content-center">
                    <div class="col-12 col-sm-10 col-md-8">
                        <input type="number" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}" placeholder="Telepon">
                        @if ($errors->has('phone'))
                            <div class="invalid-feedback">
                                {{ $errors->first('phone') }}
                            </div>
                        @endif
                    </div>
                </div>
                <div class="form-group row justify-content-center">
                    <div class="col-12 col-sm-10 col-md-8">
                        <select class="form-control select2{{ $errors->has('regency') ? ' is-invalid' : '' }}" name="regency" data-placeholder="Domisili..">
                            <option></option>
                            @foreach ($regencies as $regency)
                                <option value="{{ $regency->id }}"{{ old('regency') == $regency->id ? ' selected' : '' }}>{{ ucwords(strtolower($regency->name)) }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('regency'))
                            <div class="invalid-feedback">
                                {{ $errors->first('regency') }}
                            </div>
                        @endif
                    </div>
                </div>
                <div class="form-group row justify-content-center">
                    <div class="col-10 col-sm-8 col-md-6">
                        @if (app('request')->input('provider'))
                            @if (app('request')->input('provider') == 'facebook')
                                <button type="submit" class="btn btn-primary btn-block">
                                    Daftar Akun dengan&nbsp;&nbsp;<i class="fa fa-facebook align-middle"></i>
                                </button>
                            @elseif (app('request')->input('provider') == 'google')
                                <button type="submit" class="btn btn-danger btn-block">
                                    Daftar Akun dengan&nbsp;&nbsp;<i class="fa fa-google-plus align-middle"></i>
                                </button>
                            @endif
                        @else
                            <button type="submit" class="btn btn-danger btn-block">
                                Daftar Akun
                            </button>
                        @endif
                    </div>
                </div>
                <div class="form-group last">
                    <div class="row justify-content-center mb-0 py-3">
                        <div class="col-12 col-sm-10 col-md-8">
                            <div class="not-yet text-center">
                                Sudah memiliki akun? <a class="btn btn-link p-0" href="{{ route('login') }}">Masuk</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('page_script')
<script type="text/javascript">
    $(document).ready(function() {
        @if (app('request')->input('birth_date'))
            $('.datepicker').datepicker('option', 'disabled', true);
        @endif
    });

    $(".select2[name='gender']").select2({
        theme: 'bootstrap',
        placeholder: 'Jenis Kelamin..',
        allowClear: true
    });

    $(".select2[name='regency']").select2({
        theme: 'bootstrap',
        placeholder: 'Domisili..',
        allowClear: true
    });

    $('.datepicker').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd M yy',
        yearRange: '1950:2001',
        defaultDate: '01 Jan 1950'
    });
</script>
@endsection
