@extends('layouts.app')

@section('content')
<div class="head">
    <div class="container text-center">
        <h1><span>ANGKAT</span>&nbsp;<br class="d-sm-none"><span>SUARA ANDA</span></h1>
        <p>
            Suarakan Aspirasi dalam Demokrasi.<br>
            Aspirasi adalah jejaring sosial dan politik dimana suaramu bisa didengar
        </p>
        <div class="row justify-content-center">
            <div class="col-8 mb-3 mb-sm-0 col-sm-5 col-md-4 col-lg-3">
                <button type="button" class="btn btn-primary btn-block"
                    onclick="location.href='{{ route('social.redirect', ['provider' => 'facebook']) }}'">
                    Masuk dengan&nbsp;&nbsp;<i class="fa fa-facebook align-middle"></i>
                </button>
            </div>
            <div class="col-8 col-sm-5 col-md-4 col-lg-3">
                <button type="button" class="btn btn-danger btn-block"
                    onclick="location.href='{{ route('social.redirect', ['provider' => 'google']) }}'">
                    Masuk dengan&nbsp;&nbsp;<i class="fa fa-google-plus align-middle"></i>
                </button>
            </div>
        </div>
    </div>
    <a class="next" href="javascript:;">
        <div class="text-center">
            <i class="fa fa-chevron-down"></i>
        </div>
    </a>
</div>
<div class="what">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-md-11 col-lg-10 col-xl-9">
                <div class="row justify-content-center">
                    <div class="col-12 col-sm-7 col-md-6 mb-4 mb-sm-0 text-center text-sm-left title">
                        <div>APA ITU</div>
                        <div>ASPIRASI?</div>
                    </div>
                    <div class="col-10 col-sm-5 col-md-6 text-center text-sm-left align-self-center desc">
                        Aspirasi merupakan jejaring sosial pertama di Indonesia yang membahas secara spesifik bidang sosial, politik dan ekonomi.<br><br>
                        Aspirasi bertujuan untuk memberikan kanal demokrasi baru yang terpadudan meningkatkan kesadaran penggunanya.
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="why">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-11 col-xl-9">
                <div class="row justify-content-center">
                    <div class="col-6 d-none d-md-block">
                        <img class="img-fluid" src="{{ asset('img/welcome/kenapa.png') }}">
                    </div>
                    <div class="col-12 col-md-6 title">
                        <div class="top text-center">
                            <div><span class="d-inline-block">KENAPA ADA</span></div>
                            <div><span class="d-inline-block">ASPIRASI?</span></div>
                        </div>
                        <div class="bot">
                            <ul class="list-unstyled">
                                <li class="media">
                                    <img class="img-fluid rounded-circle" src="{{ asset('img/welcome/1.png') }}">
                                    <div class="media-body align-self-center">
                                        Kesadaran dan kepedulian rakyat Indonesia yang rendah akan politik.
                                    </div>
                                </li>
                                <li class="media">
                                    <img class="img-fluid rounded-circle" src="{{ asset('img/welcome/2.png') }}">
                                    <div class="media-body align-self-center">
                                        Penurunan kualitas demokrasi di Indonesia yang semakin mengkhawatirkan.
                                    </div>
                                </li>
                                <li class="media">
                                    <img class="img-fluid rounded-circle" src="{{ asset('img/welcome/3.png') }}">
                                    <div class="media-body align-self-center">
                                        Indeks demokrasi Indonesia dibangun dari tiga aspek, yaitu: kebebasan sipil, hak-hak politik, dan lembaga demokrasi yang mengalami penurunan.
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="top text-center">
                            <div><span class="d-inline-block">CARA KERJA</span></div>
                            <div><span class="d-inline-block">ASPIRASI?</span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="how">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-md-11 col-lg-10 col-xl-8">
                <div class="row justify-content-center">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="num float-left">
                                    1.
                                </div>
                                <div class="desc">
                                    <p>Memberikan edukasi bagi masyarakat terkait kondisi terkini dalam bidang sosial, ekonomi, dan politik.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="num float-left">
                                    2.
                                </div>
                                <div class="desc">
                                    <p>Memudahkan masyarakat dalam menyuarakan aspirasi yang mereka miliki.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card mb-3 mb-md-0">
                            <div class="card-body">
                                <div class="num float-left">
                                    3.
                                </div>
                                <div class="desc">
                                    <p>Meningkatkan interaksi antara masyarakat dan lembaga demokrasi sehingga hak-hak demokrasi mereka dapat teraktualisasi.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card mb-0">
                            <div class="card-body">
                                <div class="num float-left">
                                    4.
                                </div>
                                <div class="desc">
                                    <p>Meyakinkan masyarakat untuk ikut berkontribusi dalam proses demokrasi.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="participate">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-11 col-xl-10">
                <div class="row justify-content-center">
                    <div class="col-12 title">
                        <div><span class="d-inline-block">PARTISIPASI UNTUK</span></div>
                        <div><span class="d-inline-block">ASPIRASI</span></div>
                    </div>
                    <div class="col-12 content">
                        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li class="rounded-circle active" data-target="#carouselExampleIndicators" data-slide-to="0"></li>
                                <li class="rounded-circle" data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                <li class="rounded-circle" data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img class="d-block w-100" src="{{ asset('img/welcome/tokoh.png') }}">
                                    <div class="carousel-caption text-right">
                                        <h1>Tokoh</h1>
                                        <p class="d-none d-sm-block">Aspirasikan tokoh pilihanmu, demokratis untuk masa depan Indonesia.</p>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="{{ asset('img/welcome/tokoh.png') }}">
                                    <div class="carousel-caption text-right">
                                        <h1>Tokoh</h1>
                                        <p class="d-none d-sm-block">Aspirasikan tokoh pilihanmu, demokratis untuk masa depan Indonesia.</p>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="{{ asset('img/welcome/tokoh.png') }}">
                                    <div class="carousel-caption text-right">
                                        <h1>Tokoh</h1>
                                        <p class="d-none d-sm-block">Aspirasikan tokoh pilihanmu, demokratis untuk masa depan Indonesia.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page_script')
<script type="text/javascript">
    $(document).ready(function() {
        $('a.next').click(function() {
            $('html, body').animate({
                scrollTop: $('div.what').offset().top
            }, 750);
        });
    });
        function abs() {
        }
</script>
@endsection
